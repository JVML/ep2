# Aqui estão as informações do meu EP2 de Orientação ao Objeto.
# Aluno: João Vítor ML
# Matrícula: 160010195

# Instruções

# Assim que o usuário iniciar o programa(rodar a main), uma janela de menu irá aparecer com 3 opções:

# 1) Simulação 1: Fluxo de potência harmônica - Selecionada essa opção, será aberta uma segunda janela, que irá gerar a simulação. O usuário insere os dados nos campos adequados e aperta em SIMULAR.
# 2) Simulação 2: Distorção harmônica - Selecionada essa opção, será aberta uma segunda janela, que irá gerar a simulação 2. É recomendado o usuário inserir primeiro o número de ordens harmônicas e apertar "OK", para depois inserir todos os valores e apertar de novo o "OK" para começar a simulação.
# 3) Sair: o usuário sai do programa.
